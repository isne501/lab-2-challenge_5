using namespace std;

#ifndef Binary_Search_Tree
#define Binary_Search_Tree

template<class T> class Tree;

template<class T>
class Node
{
	public:
			Node() { left = right = NULL; }
			Node(const T& el, Node *l = 0, Node *r = 0)
			{
				key = el; left = l; right = r;
			}
			T key;
			Node *left, *right;
};

template<class T>
class Tree 
{
	public:
		Tree() { root = 0; arr = new int[500]; } //create new arr variable type array to get key from tree store in array
		~Tree() { clear(); }
		void clear() { clear(root); root = 0; }
		bool isEmpty() { return root == 0; }
		void inorder() { inorder(root); } //inorder function
		int *arr; //array
		void insert(const T& el);
		void deleteNode(Node<T> *& node);
		void visit(Node<T> *p); //function to visit the node and print it
		int height() { return height(root); } //function find height of tree
		bool search(int key) { return search(root,key); } //function search element
		void storeintoArr() { storeintoArr(root); } //function to make value from tree store in array
		void balance(T data[], int first, int last); //function that balance the tree
		void deleteN(T key) { deleteN(root, key); } //function that find the key and delete it from tree

	protected:
		Node<T> *root;
		int index = 0; //index array
		void storeintoArr(Node<T> *root);
		void clear(Node<T> *p);
		void inorder(Node<T> *p);
		void deleteN(Node<T>* p, T key);
		int height(Node<T> *p);
		bool search(Node<T> *root,int key);
};

template<class T>
void Tree<T>::clear(Node<T> *p)
{
	if (p != 0) 
	{
		clear(p->left);
		clear(p->right);
		delete p;
	}
}

template<class T>
void Tree<T>::inorder(Node<T> *p) 
{
	if (p != 0) //Recursive to see the every node
	{
		inorder(p->left);  //See the left child
		visit(p); //See  themself
		inorder(p->right);//See the right child
	}
}

template<class T>
void Tree<T>::insert(const T &el) 
{
	Node<T> *p = root, *prev = 0;
	while (p != 0) 
	{
		prev = p;
		if (p->key < el)
			p = p->right;
		else
			p = p->left;
	}
	if (root == 0)
		root = new Node<T>(el);
	else if (prev->key < el)
		prev->right = new Node<T>(el);
	else
		prev->left = new Node<T>(el);
}

template<class T>
void Tree<T>::deleteNode(Node<T> *&node) 
{
	Node<T> *prev, *tmp = node;
	if (node->right == 0)
		node = node->left;
	else if (node->left == 0)
		node = node->right;
	else 
	{
		tmp = node->left;
		prev = node;
		while (tmp->right != 0) 
		{
			prev = tmp;
			tmp = tmp->right;
		}
		node->key = tmp->key;
		if (prev == node)
			prev->left = tmp->left;
		else prev->right = tmp->left;
	}
	delete tmp;
}

//Show what node key store
template<class T>
void Tree<T>::visit(Node<T> *p)
{
	cout << p->key<<" "; //output the key that node store
}

template<class T>
int Tree<T>::height(Node<T> *p)
{
	int left_depth , right_depth;
	if (p == NULL) //if tree is empty
		return 0; //return height equal 0
	else
	{
		left_depth = height(p->left); //go depth of left and recursive 
		right_depth = height(p->right); //go depth of right and recursive 
		
		if (left_depth > right_depth)  //if depth of left more than right depth that
			return left_depth + 1; //Plus one each recursive and return the final value until no runtime stack
		else 
			return right_depth + 1;//Plus one each recursive and return the final value until no runtime stack
	}
}

template<class T>
bool Tree<T>::search(Node<T>* root, int key)
{
	while (root != 0) //loop for finding the key
	{
		if (root->key == key) //if node has key is same as the key that pass to function
			return true; //if there is return true 
		if (key > root->key) //if key more than key that node store
			root = root->right; //Move pointer to the right
		else
			root = root->left; //Move pointer to the left
	}
		return false; //if there isn't return false
	
	
}

template<class T>
void Tree<T>::balance(T data[], int first, int last) 
{
	if (first <= last) 
	{
		int middle = (first + last) / 2; //After inorder the data will sort already then find the middle of array
		insert(data[middle]); //insert middle value
		
		//Recursive to insert every value in array to the tree
		balance(data, first, middle - 1); 
		balance(data, middle + 1, last);
	}
	
}

template<class T>
void Tree<T>::storeintoArr(Node<T>* p)
{
	if (p != 0) //Recursive to see the every node
	{
		storeintoArr(p->left);  //Visit the left child
		arr[index] = p->key;  //Store value from node tree to array
		index++; //plus one each round to make uniform index
		storeintoArr(p->right);//Visit the right child
	}
}

//Delete node
template<class T>
void Tree<T>::deleteN(Node<T>* p, T key)
{
	Node<T> *tmp = p, *prev = 0;  //Make tmp point to root and prev point to null
	while (tmp != 0)
	{
		if (tmp->key == key) //if found the key break this loop tmp will point at node that has key same value as T key 
			break;
		prev = tmp; //Store previous node before moving the tmp pointer
		if (tmp->key < key) //If key to find more than node key move tmp pointer to visit the right child of node that tmp pointing
			tmp = tmp->right;
		else
			tmp = tmp->left; //move tmp pointer to visit the left child of node that tmp pointing
	}
	if (tmp != 0 && tmp->key == key) 
	{
		if (tmp == p) //if tmp same as root
			deleteNode(p); //delete root
		else if (prev->left == tmp) //if tmp live in the left child from parent delete node from prev->left the parent node will still live but child gone 
			deleteNode(prev->left);
		else
			deleteNode(prev->right); //if tmp live in the left child from parent delete node from prev->right the parent node will still live but child gone
	}
	else if (p != 0) //Not found the key to delete
		cout << "Key " << key << " was not found ";
	else //if root null that means tree is empty
		cout << "Tree is Empty! ";
}

#endif // Binary_Search_Tree