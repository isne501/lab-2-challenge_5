#include <iostream>
#include <string>
#include "tree.h"
#include <cstdlib>
#include <ctime>
#include <vector>

using namespace std;

bool isAlreadyAdded(vector<int> number, int x); //Check duplicate element

int main()
{
	Tree<int> mytree; //Create the tree
	vector<int> number; //To store the random number that help check duplicate after
	int num_search, num_insert, num,round,i = 0;;
	
	srand(time(0));
	int x = rand() % 1000; //random number between 0-999
	
	//input 500 numbers to the tree
	while (number.size() != 500) 
	{
		if (!isAlreadyAdded(number, x)) //check duplicate
		{
			number.push_back(x); //push the element to vector
			mytree.insert(x); //insert to tree
		}
		else
		{
			x = rand() % 1000; //random number
		}
	}
	//Show height of tree
	cout << "The height of Tree : " << mytree.height() << endl;
	//Show element in the tree 
	cout << "\nShow tree inorder..." << endl;
	mytree.inorder();
	cout << endl;
	//Store element from tree to array
	mytree.storeintoArr();
	//Clear the tree to make tree empty
	mytree.clear();
	//Balance the tree by pass the mytree.arr that form mytree.storeintoArr();
	mytree.balance(mytree.arr,0,499);
	//Show height after balancing
	cout << "\nThe height of tree after calling balance : " << mytree.height() << endl;
	
	//Output the let user decide how many insert
	cout << "\nHow many 'insertion' operations should be done : ";
	cin >> num_insert;

	//Loop for let user decide number insert to the tree each insert
	while (i < num_insert)
	{
		cout << "Insert" << " [" << i + 1 << "] " << " Input number : ";
		cin >> num;
		while (mytree.search(num)) //check duplicate
		{
			cout << "There is in the tree INPUT again : ";
			cin >> num;
		}
		mytree.insert(num); //insert to tree
		i++;
	}
	cout << endl;
	//show height after insert by user
	cout << "\nThe height of tree after insert operation : " << mytree.height() << endl;

	//Output to let the user decide how many deleteion be performed
	cout << "\nHow many 'deletion' operations should be performed : ";
	cin >> round;
	
	//Loop for output each delete round 
	i = 0;
	while (i < round)
	{
		x = rand() % 1000; //random number to delete
		if (mytree.search(x) == false) //Check Is number in the tree ? if not go the next round
		{
			cout << "Delete" << " [" << i + 1 << "] " << "Try delete number " << x << " result : There isn't in the tree" << endl;
			i++;
			continue;
		}
		if (mytree.search(x) == true) //If numbere is in the tree delete it
		{
			mytree.deleteN(x); //delete number from tree
			cout << "Delete" << " [" << i + 1 << "] " << "Try delete number " << x << " result : Success" << endl;
		}
		i++;
	}
	cout << endl;
	//Show tree after delete 
	cout << "Show tree after deleted..." << endl;
	mytree.inorder();
	cout << endl;
	//Output to let user decide number to search in the tree
	cout << "\nInput number to search : ";
	cin >> num_search;
	if (mytree.search(num_search))
	{
		cout << "\nNumber " << num_search <<" was found!\n" ;
	}
	else
		cout << "\nNumber " << num_search << " wasn't found!\n";

	system("PAUSE");	
		return 0;

}

//Check the element for prevent duplicate
bool isAlreadyAdded(vector<int> number, int x)
{
	for (int i = 0; i < number.size(); i++)
		if (number.at(i) == x)
			return true;
	return false;
}